import matplotlib.pyplot as plt
import pandas as pd
import matplotlib
import numpy as np
import seaborn as sns


kom = pd.read_excel('komar.xlsx')
y_pos = np.arange(len(kom['Ilosc']))
sns.set()
 

#rysyuje dwa lolipopy
plt.subplot(211)

# Reorder its following the values:
ordered_df = kom.sort_values(by='Ilosc')
my_range=range(len(kom.index))

ordered_df1 = ordered_df[12:15]
ordered_df2 = ordered_df[4:13]
my_range1 = range(len(ordered_df1['Ilosc']))

my_range2 = range(len(ordered_df2['Ilosc']))




plt.hlines(y=my_range1, xmin=0, xmax=ordered_df1['Ilosc'])
plt.plot(ordered_df1['Ilosc'], my_range1, "o")
plt.yticks(my_range1, ['Mosquito', 'Human', 'Snake'])
plt.title("The most dangerous animals", fontsize='xx-large')
plt.xlim(0, 750000)
plt.subplot(212)

#plt.subplots(figsize=(10, 10))

plt.hlines(y=my_range2, xmin=0, xmax=ordered_df2['Ilosc'])
plt.plot(ordered_df2['Ilosc'], my_range2, "o")
 
# Add titles and axis names
plt.yticks(my_range2, ['Snake', 'Dog', 'Tse-tse', 'Reduviidae', 'Freshwater snail', 'Large roundworm', 'Cestoda' , 'Crocodile', 'Hippo'])
plt.xlim(0, 55000)
plt.xlabel('Deaths in 2014')
plt.tight_layout()
fig = plt.gcf()
fig.set_size_inches(10, 6)
plt.savefig('animals.pdf')
plt.show()

ad = pd.read_excel('Zeszyt1.xlsx')
plt.plot(ad['Rok'],ad['Maksymalnie 1 ITN'],'-o', label='max 1 ITN')
plt.plot(ad['Rok'],ad['Dostęp do ITN'],'-o', label='access to ITN')
plt.plot(ad['Rok'],ad['Populacja śpiąca pod ITN'],'-o', label='sleeping under ITN')
plt.plot(ad['Rok'],ad['Wystarczająca ilośc ITN dla każdego członka gospodarstwa'],'-o', label='ITN availibility for every member')
plt.legend()
plt.ylim([0,100])
plt.xlim(2010, 2017)
plt.title("ITN access in Subsaharian Africa", fontsize="xx-large")
plt.ylabel('Percentage of households')
plt.xlabel("Years")
plt.savefig('itn.pdf')
fig = plt.gcf()
fig.set_size_inches(13, 6)
plt.show()