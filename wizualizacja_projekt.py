import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

data2 = pd.read_csv("https://apps.who.int/gho/athena/data/GHO/MALARIA003?filter=COUNTRY:-;REGION:*&x-sideaxis=REGION&x-topaxis=GHO;YEAR&profile=crosstable&format=csv", header=1)

def my_int(something):
    if '[' in something:
      something = something.split('[')[0]
    try:
      return int(something)
    except:
      return something

data2 = data2.applymap(my_int)

years = list(data2.columns[1:])
y = np.arange(len(list(data2.iloc[0, 1:])))

world = [sum([data2.iloc[i, j] for i in range(6)]) for j in range(1, 9)]
all_ = 8 * [0]

for i in range(len(data2)-1):
  country = list(data2.iloc[i, 1:])
  values = [country[i]/world[i] for i in range(8)]
  plt.barh(y, values, left=all_)
  all_ = [a+b for (a,b) in zip(all_, values)]

sns.set_style("whitegrid")
sns.despine(left=True)
plt.yticks(y, years)
plt.xticks([])
plt.xlim(0, 1)
plt.grid()
plt.legend(list(data2.iloc[:, 0]), framealpha=0.94)
plt.title("Percentage share of WHO regions in estimated deaths due to malaria", fontsize="xx-large")
fig = plt.gcf()
fig.set_size_inches(14, 2)
plt.savefig("continents_evolution.pdf")
plt.show()

data = pd.read_csv("https://apps.who.int/gho/athena/data/xmart.csv?target=GHO/MALARIA001&profile=crosstable&filter=COUNTRY:*&x-sideaxis=COUNTRY&x-topaxis=GHO;YEAR&x-collapse=true")
data.columns = ['Country'] + list(range(2017, 1999, -1))
data = data.fillna(0)
data['avg'] = data.mean(axis=1)
data.sort_values(by=['avg'], inplace=True, ascending=False)

n = 9
sns.set(color_codes=True)
x = list(data.columns[1:-1])
y = [list(data.iloc[i][1:-1]) for i in range(0, 105)]
other = [[sum(y[val][i] for val in range(n, 105)) for i in range(18)]]
y = other + y[:n]
l = ['other countries'] + list(data.iloc[:n, 0])

plt.stackplot(x, y, labels=l)

plt.ylim(0, 165000)
plt.xlim(2000, 2017)
fig = plt.gcf()
fig.set_size_inches(10, 6)
plt.legend(framealpha=0.4)
plt.title("Reported deaths due to malaria by country", fontsize="xx-large")
plt.savefig("stackplot.pdf")
